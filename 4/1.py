def load_cards():
    cards = []

    with open("./adventofcode.com_2023_day_4_input.txt") as input_file:
        while True:
            line = input_file.readline()

            if line == '':
                break
            
            line = line.split(':')[1].split('|')

            cards.append({
                "winning": map(lambda x: int(x), line[0].strip().split()),
                "numbers": map(lambda x: int(x), line[1].strip().split()),
            })

    return cards

def card_score(card):
    winning = set(card["winning"])
    numbers = set(card["numbers"])

    matches = winning & numbers
    len_matches = len(matches)

    if len_matches >= 1:
        return 2 ** (len_matches - 1)
    
    return 0

def main():
    cards = load_cards()
    sum = 0

    for card in cards:
        sum += card_score(card)

    print(sum)




if __name__ == "__main__":
    main()