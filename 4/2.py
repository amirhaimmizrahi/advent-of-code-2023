def load_cards():
    cards = {}

    with open("./adventofcode.com_2023_day_4_input.txt") as input_file:
    # with open("./test.txt") as input_file:
        while True:
            line = input_file.readline()

            if line == '':
                break
            
            id = int(line.split(':')[0][4:])
            line = line.split(':')[1].split('|')

            cards[id] = {
                "winning": map(lambda x: int(x), line[0].strip().split()),
                "numbers": map(lambda x: int(x), line[1].strip().split()),
            }

    return cards

def num_of_card_matches(card):
    winning = set(card["winning"])
    numbers = set(card["numbers"])

    matches = winning & numbers
    return len(matches)

def calc_card_copies(card_id, card, highest_card_id):
    num_of_matches = num_of_card_matches(card)
    copies = set()

    for i in range(num_of_matches if num_of_matches + card_id < highest_card_id else highest_card_id - card_id):
        copies.add(card_id + i + 1)

    return copies



def main():
    cards = load_cards()
    highest_card_id = len(cards)
    card_copies = {}

    for (card_id, card) in cards.items():
        card_copies[card_id] = calc_card_copies(card_id, card, highest_card_id)
        print(card_id, card_copies[card_id])

    stack = list(cards.keys())
    num_of_copies = 0

    while len(stack):
        num_of_copies += 1
        card_id = stack[-1]
        stack.pop()
        
        copies = card_copies[card_id]

        for copy_id in copies:
            stack.append(copy_id)

    print(num_of_copies)
            
if __name__ == "__main__":
    main()