def load_input():
    # content = open('./example.txt').read()
    content = open('./input.txt').read()
    seeds, *conversion_maps = content.split('\n\n')

    seeds = tuple(int(x) for x in seeds.split(':')[1].split())
    seeds = tuple(zip(seeds[::2], seeds[1::2]))
    
    conversion_maps = tuple(tuple(tuple(map(lambda x: int(x), conversion.split())) for conversion in conversion_map.strip().split('\n')[1:]) for conversion_map in conversion_maps)
    
    return seeds, conversion_maps

def seed_to_location(conversion_maps, seed):
    stack = [seed]
    next_stack = []
    
    for conversion_map in conversion_maps:
        for seed_start, seed_length in stack:
            seed_end = seed_start + seed_length - 1
            
            for dest_start, src_start, length in conversion_map:
                src_end = src_start + length - 1
                
                # seed[---==========---  ]
                # src [   ----------     ]
                
                # seed[---=============  ]
                # src [   ---------------]
                if seed_start <= src_start and seed_end > src_start:
                    next_stack.append((dest_start, min(seed_end, src_end) - src_start))
                    
                # seed[   ==========---  ]
                # src [-------------     ]
                
                # seed[   =============  ]
                # src [------------------]
                elif seed_start > src_start and seed_start <= src_end:
                    next_stack.append((dest_start + seed_start - src_start, min(seed_end, src_end) - seed_start))
                    
            if len(next_stack) == 0:
                next_stack.append((seed_start, seed_length))
            
        stack = [*next_stack]
        next_stack = []
        
    return stack

def main():
    seeds, conversion_maps = load_input()
    
    
    locations = []
    for seed in seeds:
        locations += seed_to_location(conversion_maps, seed)

    print(min(locations)[0])
if __name__ == '__main__':
    main()