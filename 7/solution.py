import functools

def load_input():
    # content = open('./example.txt').read()
    content = open('./input.txt').read()
    content = content.strip().split('\n')
    hands = tuple(map(lambda line: line.split()[0], content))
    bids = tuple(map(lambda line: int(line.split()[1]), content))

    return hands, bids

def hand_strength(hand):
    cards = {}

    for card in hand:
        if card not in cards:
            cards[card] = 0

        cards[card] += 1

    jokers_count = 0
    
    if 'J' in cards:
        jokers_count = cards['J']
        del cards['J']

    occurences = sorted(cards.values(), reverse=True)
    if len(occurences):
        occurences[0] += jokers_count
    else:
        occurences.append(5)
    #five of a kind
    if occurences[0] == 5:
        return 7
    
    #four of a kind
    if occurences[0] == 4:
        return 6
    
    #full house
    if occurences[0] == 3 and occurences[1] == 2:
        return 5
    
    #three of a kind
    if occurences[0] == 3:
        return 4
    
    #two pairs
    if occurences[0] == 2 and occurences[1] == 2:
        return 3
    
    #pair
    if occurences[0] == 2:
        return 2
    
    #high card
    return 1

def card_strength(card):
    if card == 'J':
        return 0
    
    if card == 'T':
        return 10
    
    if card == 'Q':
        return 11
    
    if card == 'K':
        return 12
    
    if card == 'A':
        return 13
    
    return int(card)

def rank_hands(hands, bids):
    def compare_hands(hand0, hand1):
        hand0 = hand0[0]
        hand1 = hand1[0]

        hand0_strength = hand_strength(hand0)
        hand1_strength = hand_strength(hand1)

        if hand0_strength > hand1_strength:
            return 1
        if hand1_strength > hand0_strength:
            return -1
        
        for card0, card1 in zip(hand0, hand1):
            card0_strength, card1_strength = card_strength(card0), card_strength(card1)

            if card0_strength > card1_strength:
                return 1
            
            if card1_strength > card0_strength:
                return -1
            
        return 0

    return sorted(zip(hands, bids), key=functools.cmp_to_key(compare_hands))

def main():
    hands, bids = load_input()

    ranked_hands = rank_hands(hands, bids)

    sum = 0

    for rank, (hand, bid) in enumerate(ranked_hands):
        sum += bid * (rank + 1)

    print(sum)
if __name__ == '__main__':
    main()