
def parse_line(line):
    data = {}

    line = line.split(':')
    data['id'] = int(line[0][5:])
    data['reveals'] = []

    line = line[1].split(';')

    for reveal in line:
        reveal_data = {'red': 0, 'blue': 0, "green": 0}
        reveal = reveal.split(',')

        for cube in reveal:
            cube = cube.strip().split(' ')
            reveal_data[cube[1]] = int(cube[0])

        data['reveals'].append(reveal_data)

    return data

def is_game_possible(game_data):
    AMOUNT_OF_REDS = 12
    AMOUNT_OF_GREENS = 13
    AMOUNT_OF_BLUES = 14

    for reveal in game_data['reveals']:
        if (reveal['red'] > AMOUNT_OF_REDS or \
            reveal['green'] > AMOUNT_OF_GREENS or \
            reveal['blue'] > AMOUNT_OF_BLUES):
            return False

    return True

def main():
    sum = 0

    with open("./adventofcode.com_2023_day_2_input.txt") as input_file:
        while True:
            line = input_file.readline()

            if line == '':
                break
            
            game_data = parse_line(line)

            if is_game_possible(game_data):
                sum += game_data['id']

    print(sum)



if __name__ == "__main__":
    main()