
def parse_line(line):
    data = {}

    line = line.split(':')
    data['id'] = int(line[0][5:])
    data['reveals'] = []

    line = line[1].split(';')

    for reveal in line:
        reveal_data = {'red': 0, 'blue': 0, "green": 0}
        reveal = reveal.split(',')

        for cube in reveal:
            cube = cube.strip().split(' ')
            reveal_data[cube[1]] = int(cube[0])

        data['reveals'].append(reveal_data)

    return data

def get_game_power(game_data):
    max_red = 0
    max_blue = 0
    max_green = 0

    for reveal in game_data['reveals']:
        if reveal['red'] > max_red:
            max_red = reveal['red']

        if reveal['blue'] > max_blue:
            max_blue = reveal['blue']

        if reveal['green'] > max_green:
            max_green = reveal['green']

    return max_red * max_blue * max_green

def main():
    sum = 0

    with open("./adventofcode.com_2023_day_2_input.txt") as input_file:
        while True:
            line = input_file.readline()

            if line == '':
                break
            
            game_data = parse_line(line)
            sum += get_game_power(game_data)

    print(sum)



if __name__ == "__main__":
    main()