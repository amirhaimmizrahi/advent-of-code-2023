def load_engine():
    engine = []

    with open("./adventofcode.com_2023_day_3_input.txt") as input_file:
        while True:
            line = input_file.readline()

            if line == '':
                break
            
            engine.append(line[:-1])

    return engine

def parse_part(line, index, line_number):
    part = {"number": 0, "start_index": 0, "end_index": 0}
    number = ''

    part['line_number'] = line_number
    part['start_index'] = index

    for ch in line[index:]:
        if ch.isdigit():
            number += ch
        else:
            break

    end_index = index + len(number) - 1

    part['end_index'] = end_index
    part['number'] = int(number)

    return end_index, part

def yield_part(engine):
    for line_number, line in enumerate(engine):
        line_len = len(line)
        i = 0

        while i < line_len:
            ch = line[i]
            
            if ch.isdigit():
                end_index, part = parse_part(line, i, line_number)
                i = end_index
                yield part

            i += 1


def get_part_neighbors(engine, part):
    neighbors = []

    y = part['line_number']
    start = part['start_index']
    end = part['end_index']
    
    if start > 0:
        neighbors.append({"symbol": engine[y][start - 1], "x": start - 1, 'y': y})

    if end < len(engine[y]) - 1:
        neighbors.append({"symbol": engine[y][end + 1], 'x': end + 1, 'y': y})

    if y > 0:
        for x in range(start, end + 1):
            neighbors.append({"symbol": engine[y - 1][x], 'x': x, 'y': y - 1})

        if start > 0:
            neighbors.append({"symbol": engine[y - 1][start - 1], 'x': start - 1, 'y': y - 1})

        if end < len(engine[y]) - 1:
            neighbors.append({"symbol": engine[y - 1][end + 1], 'x': end + 1, 'y': y - 1})

    if y < len(engine) - 1:
        for x in range(start, end + 1):
            neighbors.append({"symbol": engine[y + 1][x], 'x': x, 'y': y + 1})

        if start > 0:
            neighbors.append({"symbol": engine[y + 1][start - 1], 'x': start - 1, 'y': y + 1})

        if end < len(engine[y]) - 1:
            neighbors.append({"symbol": engine[y + 1][end + 1], 'x': end + 1, 'y': y + 1})

    return neighbors
            

def main():
    engine = load_engine()
    gears = {}

    for part in yield_part(engine):
        part_neighbors = get_part_neighbors(engine, part)
        is_part = False

        for neighbor in part_neighbors:
            if neighbor['symbol'] != '.':
                is_part = True
                break

        if is_part:
            for neighbor in part_neighbors:
                symbol = neighbor['symbol']
                x = neighbor['x']
                y = neighbor['y']

                if symbol == '*':
                    if (x, y) in gears:
                        gears[(x, y)]['count'] += 1
                        gears[(x, y)]['parts'].append(part)
                    else:
                        gears[(x, y)] = {'count': 1, 'parts': [part]}

    sum = 0

    for (position, gear) in gears.items():
        if gear['count'] == 2:
            ratio = 1
            ratio *= gear['parts'][0]['number']
            ratio *= gear['parts'][1]['number']
            
            sum += ratio

    print(sum)

if __name__ == "__main__":
    main()