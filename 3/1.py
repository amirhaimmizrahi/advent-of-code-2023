def load_engine():
    engine = []

    with open("./adventofcode.com_2023_day_3_input.txt") as input_file:
        while True:
            line = input_file.readline()

            if line == '':
                break
            
            engine.append(line[:-1])

    return engine

def parse_part(line, index, line_number):
    part = {"number": 0, "start_index": 0, "end_index": 0}
    number = ''

    part['line_number'] = line_number
    part['start_index'] = index

    for ch in line[index:]:
        if ch.isdigit():
            number += ch
        else:
            break

    end_index = index + len(number) - 1

    part['end_index'] = end_index
    part['number'] = int(number)

    return end_index, part

def yield_part(engine):
    for line_number, line in enumerate(engine):
        line_len = len(line)
        i = 0

        while i < line_len:
            ch = line[i]
            
            if ch.isdigit():
                end_index, part = parse_part(line, i, line_number)
                i = end_index
                yield part

            i += 1


def get_part_neighbors(engine, part):
    neighbors = []

    y = part['line_number']
    start = part['start_index']
    end = part['end_index']
    
    if start > 0:
        neighbors.append(engine[y][start - 1])

    if end < len(engine[y]) - 1:
        neighbors.append(engine[y][end + 1])

    if y > 0:
        for x in range(start, end + 1):
            neighbors.append(engine[y - 1][x])

        if start > 0:
            neighbors.append(engine[y - 1][start - 1])

        if end < len(engine[y]) - 1:
            neighbors.append(engine[y - 1][end + 1])

    if y < len(engine) - 1:
        for x in range(start, end + 1):
            neighbors.append(engine[y + 1][x])

        if start > 0:
            neighbors.append(engine[y + 1][start - 1])

        if end < len(engine[y]) - 1:
            neighbors.append(engine[y + 1][end + 1])

    return neighbors
            

def main():
    engine = load_engine()
    sum = 0

    for part in yield_part(engine):
        part_neighbors = get_part_neighbors(engine, part)

        for symbol in part_neighbors:
            if symbol != '.':
                sum += part['number']
                break

    print(sum)




if __name__ == "__main__":
    main()