import math

def load_input():
    # content = open('./example.txt').read()
    content = open('./input.txt').read()
    duration, records = map(lambda line : tuple(map(lambda x: int(x), line.split(':')[1].strip().split())), content.strip().split('\n'))

    return tuple(zip(duration, records))


def num_of_possible_button_hold_times(race_duration, last_record):
    min_button_hold_time = (-race_duration + (race_duration ** 2 - 4 * last_record) ** 0.5) / -2
    max_button_hold_time = (-race_duration - (race_duration ** 2 - 4 * last_record) ** 0.5) / -2

    #if both h1 and h2 are integers, we need to offset the result
    offset = int(min_button_hold_time == min_button_hold_time // 1 and max_button_hold_time == max_button_hold_time // 1) * 2
    
    min_button_hold_time = math.ceil(min_button_hold_time)
    max_button_hold_time = math.floor(max_button_hold_time)
    
    return max_button_hold_time - min_button_hold_time - offset + 1

def main():
    content = load_input()

    print(num_of_possible_button_hold_times(49979494, 263153213781851))

if __name__ == '__main__':
    main()