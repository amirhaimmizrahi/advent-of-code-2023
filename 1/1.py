def extract_number(line):
    number = ""

    for ch in line:
        if ch.isdigit():
            number += ch
            break

    for ch in line[::-1]:
        if ch.isdigit():
            number += ch
            break

    return int(number)

def main():
    sum = 0

    with open("./adventofcode.com_2023_day_1_input.txt") as input_file:
        while True:
            line = input_file.readline()

            if line == '':
                break


            sum += extract_number(line)
    
    print (sum)


if __name__ == "__main__":
    main()