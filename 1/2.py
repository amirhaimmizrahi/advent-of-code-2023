numbers_str = [
    "one",
    'two',
    'three',
    'four',
    'five',
    'six',
    'seven',
    'eight',
    'nine',
    'zero',
]

numbers_str_to_number = {
    "one": '1',
    'two': '2',
    'three': '3',
    'four': '4',
    'five': '5',
    'six': '6',
    'seven': '7',
    'eight': '8',
    'nine': '9',
    'zero': '0',
}


def is_number_str(line, i):
    for number_str in numbers_str:
        j = 0
        flag = True

        for ch in line[i:i + len(number_str)]:
            if ch != number_str[j]:
                flag = False
                break
            
            j += 1

        if flag:
            return number_str

    return None

def extract_number(line):
    number = ""

    for i, ch in enumerate(line):
        if ch.isdigit():
            number += ch
            break

        number_str = is_number_str(line, i)

        if number_str:
            number += numbers_str_to_number[number_str]
            break

    for i, ch in list(enumerate(line))[::-1]:
        if ch.isdigit():
            number += ch
            break

        number_str = is_number_str(line, i)

        if number_str:
            number += numbers_str_to_number[number_str]
            break

    return int(number)

def main():
    sum = 0

    with open("./adventofcode.com_2023_day_1_input2.txt") as input_file:
        while True:
            line = input_file.readline()

            if line == '':
                break
                
            sum += extract_number(line)
    
    print (sum)


if __name__ == "__main__":
    main()